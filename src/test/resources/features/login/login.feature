Feature: Login Page

  Background: : : Open facebook login page
    Given open login page
    When allow cookies

  Scenario: Check if URL is right
    Then check Url

  Scenario:  Check if buttons are clickable
    Then check if main buttons are clickable

  Scenario: Check login and password are right type
    Then check if login is type text
    Then check if password is type password

  Scenario: Login with bad credentials
    When type login email which is not registred
    When type password according to pw policies
    And  click on login button
    Then check if login is unsuccessful

  Scenario: Create new account
    When click on create new profile
    Then check if it shows window for register
    # Daľšie kroky by som zvolil ako je popísané nižšie, ale dané funckcie som neimplementoval:
 #   Then check if all array has right html type
 #   When fill all arrays with right data
 #   When click on Register button
 #   Then check if Registration is successful

  Scenario: Forgot password for unknown user
    When click on forgot password
    Then check if it shows new window for forgot password
    When type there some not registred e-mail
    When  click on search button
    Then check if it shows error message
