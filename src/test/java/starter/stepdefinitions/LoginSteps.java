package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.pages.LoginStepPage;

public class LoginSteps {

    @Steps
    LoginStepPage loginStepPage;

    @Given("open login page")
    public void openLoginPage(){
        loginStepPage.openLoginPage();
    }

    @When("allow cookies")
    public void allowCookies(){
        loginStepPage.allowCookies();
    }

    @When("type login email which is not registred")
    public void typeLogin(){
        loginStepPage.typeLogin();
    }

    @When("type password according to pw policies")
    public void typePassword() {
        loginStepPage.typePassword();
    }

    @When("click on login button")
    public void clickOnLoginButton(){
        loginStepPage.clickOnLoginButton();
    }

    @When("click on create new profile")
    public void clickOnRegisterButton(){
        loginStepPage.clickOnRegisterButton();
    }

    @When("click on forgot password" )
    public void clickOnForgotPwButton(){
        loginStepPage.clickOnForgotPwButton();
    }
    @When("type there some not registred e-mail")
    public void typeNotRegistredEmail(){
        loginStepPage.typeNotRegistredEmail();
    }

    @When("click on search button")
    public void clickOnSearchButton(){
        loginStepPage.clickOnSearchButton();
    }
    @Then("check Url")
    public void checkUrl(){
        loginStepPage.checkUrl();
    }

    @Then("check if main buttons are clickable")
    public void buttonsAreClickable(){
        loginStepPage.buttonsAreClickable();
    }

    @Then("check if login is type text")
    public void loginIsTypeText() {
        loginStepPage.loginIsTypeText();
    }

    @Then("check if password is type password")
    public void passwordIsTypePassword(){
        loginStepPage.passwordIsTypePassword();
    }

    @Then("check if login is unsuccessful")
    public void loginIsUnsuccessful(){
        loginStepPage.loginIsUnsuccessful();
    }

    @Then("check if it shows window for register")
    public void registerWindowIsVisible(){
        loginStepPage.registerWindowIsVisible();
    }

    @Then("check if it shows new window for forgot password")
    public void windowForgotPwIsVisible(){
        loginStepPage.windowForgotPwIsVisible();
    }

    @Then("check if it shows error message")
    public void errorMessageIsVisible(){
        loginStepPage.errorMessageIsVisible();
    }
}
