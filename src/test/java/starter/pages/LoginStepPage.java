package starter.pages;

import net.thucydides.core.annotations.Step;

public class LoginStepPage {

    LoginPage loginPage;

    @Step
    public void openLoginPage() {
        loginPage.open();

    }
    @Step
    public void allowCookies(){
         loginPage.allowCookies();
    }

    @Step
    public void checkUrl(){
        loginPage.checkUrl();
    }

    @Step
    public void buttonsAreClickable(){
        loginPage.buttonsAreClickable();
    }

    @Step
    public void loginIsTypeText(){

        loginPage.elementHasRightType("text", "email");
    }
    @Step
    public void passwordIsTypePassword(){
        loginPage.elementHasRightType("password", "pass");
    }
    @Step
    public void typeLogin(){
        loginPage.typeLogin();
    }
    @Step
    public void typePassword() {
        loginPage.typePassword();
    }
    @Step
    public void clickOnLoginButton() {
        loginPage.clickOnButton("loginButton");{
        }
    }
    @Step
    public void loginIsUnsuccessful() {
        loginPage.loginIsUnsuccessful();
    }
    @Step
    public void clickOnRegisterButton() {
        loginPage.clickOnButton("registerButton");
    }
    @Step
    public void registerWindowIsVisible() {
        loginPage.registerWindowIsVisible();
        }
    @Step
    public void clickOnForgotPwButton() {
        loginPage.clickOnButton("forgotPwButton");
    }
    @Step
    public void windowForgotPwIsVisible() {
        loginPage.windowForgotPwIsVisible();
    }
    @Step
    public void typeNotRegistredEmail() {
        loginPage.typeNotRegistredEmail();
    }
    @Step
    public void clickOnSearchButton() {
        loginPage.clickOnButton("searchButton");
    }
    @Step
    public void errorMessageIsVisible() {
        loginPage.errorMessageIsVisible();
    }
}

