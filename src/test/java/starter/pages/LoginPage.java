package starter.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.facebook.com/")
public class LoginPage extends PageObject {

    SoftAssertions softAssertions = new SoftAssertions();

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/div/div/div/div[3]/button[2]")
    private WebElementFacade cookiesSubmitButton;

    @FindBy(name = "login")
    private WebElementFacade loginButton;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div[1]/form/div[3]/a")
    private WebElementFacade forgotPwButton;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div[1]/form/div[5]/a")
    private WebElementFacade registerButton;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div[1]/div/div/div/div[2]/div/div[2]/a")
    private WebElementFacade registerButtonForCelebrities;

    @FindBy(id = "email")
    private WebElementFacade loginArray;

    @FindBy(id = "pass")
    private WebElementFacade passwordArray;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div[1]/div/div[2]/div/div/form/div/div[2]/div/table/tbody/tr[2]/td[2]/input")
    private WebElementFacade forgotEmailArray;

    @FindBy(xpath = "/html/body/div[1]/div[2]/div[1]/div/div[2]/div/div/form/div/div[3]/div/div[1]/button")
    private WebElementFacade searchButton;

    public void allowCookies(){
        clickOn(cookiesSubmitButton);
    }

    public void checkUrl(){
        Assert.assertEquals("Error ! Login page URL is not equal! ",
                this.getClass().getAnnotation(DefaultUrl.class).value(),
                getDriver().getCurrentUrl());

    }

    public void buttonsAreClickable(){
            softAssertions.assertThat(registerButtonForCelebrities.isClickable());
            softAssertions.assertThat(registerButton.isClickable());
            softAssertions.assertThat(forgotPwButton.isClickable());
            softAssertions.assertThat(loginButton.isClickable());
        softAssertions.assertAll();
    }

    public void elementHasRightType(String expectedType, String idOfElement){
            softAssertions.assertThat(find(By.id(idOfElement)).getAttribute("type")).isEqualTo(expectedType);
        softAssertions.assertAll();
    }

    public void typeLogin() {
        loginArray.type("autotester@autotester.com");
    }

    public void typePassword() {
        passwordArray.type("Autotester!123");
    }

    public void clickOnButton(String button) {
        switch (button) {
            case "loginButton":
                clickOn(loginButton);
                break;
            case "registerButton":
                clickOn(registerButton);
                break;
            case "forgotPwButton":
                clickOn(forgotPwButton);
                break;
            case "searchButton":
                clickOn(searchButton);
                break;
            default:
                System.out.println("Button does not exist, check your string!");
        }
    }

    public void loginIsUnsuccessful() {
            softAssertions.assertThat
                (find(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div[2]/form/div/div[1]/div[2]"))
                        .isVisible());
        softAssertions.assertAll();
    }

    public void registerWindowIsVisible() {
        softAssertions.assertThat
                (find(By.xpath("/html/body/div[3]/div[2]/div/div/div[1]/div[1]"))
                        .isVisible());
        softAssertions.assertAll();
    }

    public void windowForgotPwIsVisible() {
        softAssertions.assertThat
                (find(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div/div/form/div/div[1]/div/div[2]/h2"))
                        .isVisible());
        softAssertions.assertAll();
    }

    public void typeNotRegistredEmail() {
        forgotEmailArray.type("autotester@autotester.com");
    }

    public void errorMessageIsVisible() {
        softAssertions.assertThat
                (find(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div[2]/div/div/form/div/div[2]/div[1]/div[1]"))
                        .isVisible());
        softAssertions.assertAll();
    }
}
